# TRV Control

A small program to control thermostatic radiator valves using zigbee2mqtt. The idea is, that you write a configuration
file specifying profiles, which get activated on a specified schedule.

# Installation

```shell
cargo install --locked --git https://github.com/madmo/trvcontrol
```

# Usage

Sample config:

```toml
[mqtt]
host = "localhost"
port = 1883
user = "user"
password = "password"

[profiles.workday_morning]
alarm = { days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"], time = 04:00:00 }
temperature = { bath_trv = 23.0, bedroom_trv = 21.0 }

[profiles.workday_day]
alarm = { days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"], time = 08:00:00 }
temperature = { bath_trv = 20.0, office_trv = 23 }
```

Starting the program:

```shell
trvcontrol config.toml
```

# License

Distributed under the ISC License. See LICENSE.txt for more information.