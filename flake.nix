{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    import-cargo.url = "github:edolstra/import-cargo";
  };

  outputs = { self, nixpkgs, flake-utils, import-cargo }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        inherit (import-cargo.builders) importCargo;
      in {
        packages.default = pkgs.stdenv.mkDerivation {
          name = "trvcontrol";
          src = self;

          nativeBuildInputs = with pkgs; [
            # setupHook which makes sure that a CARGO_HOME with vendored dependencies
            # exists
            (importCargo {
              lockFile = ./Cargo.lock;
              inherit pkgs;
            }).cargoHome

            # Build-time dependencies
            rustc
            cargo
          ];

          buildPhase = ''
            cargo build --release --offline
          '';

          installPhase = ''
            install -Dm775 ./target/release/trvcontrol $out/bin/trvcontrol
          '';

        };

      });
}
