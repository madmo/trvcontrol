use std::collections::HashMap;
use std::time::Duration;

use clap::Parser;
use clokwerk::Interval::{Friday, Saturday, Sunday, Thursday, Wednesday};
use clokwerk::{Interval, Job, Scheduler, SyncJob};
use log::{debug, error, warn};
use rumqttc::{Client, ClientError, MqttOptions, QoS};
use serde::Deserialize;
use snafu::{ResultExt, Whatever};
use toml::value::Datetime;

#[derive(Parser)]
struct Args {
    /// path to config file
    config: String,

    #[clap(long)]
    dump_config: bool,
}

#[derive(Deserialize)]
pub struct Config {
    mqtt: Mqtt,
    profiles: HashMap<String, Profile>,
}

#[derive(Deserialize)]
pub struct Mqtt {
    host: String,
    port: u16,
    user: Option<String>,
    password: Option<String>,
}

type FriendlyName = String;

#[derive(Deserialize, Clone)]
pub struct Profile {
    alarm: Option<Alarm>,
    temperature: HashMap<FriendlyName, f32>,
}

#[derive(Deserialize, Clone)]
pub struct Alarm {
    days: Vec<Days>,
    time: Datetime,
}

#[derive(Deserialize, Clone)]
pub enum Days {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
}

impl From<Days> for Interval {
    fn from(val: Days) -> Interval {
        match val {
            Days::Monday => Interval::Monday,
            Days::Tuesday => Interval::Tuesday,
            Days::Wednesday => Wednesday,
            Days::Thursday => Thursday,
            Days::Friday => Friday,
            Days::Saturday => Saturday,
            Days::Sunday => Sunday,
        }
    }
}

fn main() -> Result<(), Whatever> {
    let args = Args::parse();
    let config: Config = toml::from_str(std::fs::read_to_string(&args.config).unwrap().as_str())
        .with_whatever_context(|_| format!("Could not parse config file {}", args.config))?;

    let mut scheduler = Scheduler::new();

    let mut mqttoptions = MqttOptions::new("space_heater", config.mqtt.host, config.mqtt.port);
    mqttoptions.set_keep_alive(Duration::from_secs(5));
    if let (Some(user), Some(password)) = (config.mqtt.user, config.mqtt.password) {
        mqttoptions.set_credentials(user, password);
    }

    let (client, mut connection) = Client::new(mqttoptions, 10);

    for (_, profile) in config.profiles {
        handle_profile(&mut scheduler, profile, &client, args.dump_config);
    }

    let watch_thread = scheduler.watch_thread(Duration::from_millis(500));

    for notification in connection.iter() {
        match notification {
            Ok(n) => debug!("mqtt notification: {:?}", n),
            Err(e) => error!("mqtt error: {:?}", e),
        }
    }

    watch_thread.stop();

    Ok(())
}

fn handle_profile(scheduler: &mut Scheduler, profile: Profile, client: &Client, dump: bool) {
    if let Some(ref alarm) = profile.alarm {
        let mut jobq: Option<&mut SyncJob> = Option::None;

        let time = alarm.time.time.unwrap();
        let time = chrono::naive::NaiveTime::from_hms_nano_opt(
            time.hour as u32,
            time.minute as u32,
            time.second as u32,
            time.nanosecond,
        )
        .unwrap();

        for day in alarm.days.iter() {
            let day: Interval = (*day).clone().into();
            if let Some(prev) = jobq {
                jobq = Some(prev.and_every(day).at_time(time))
            } else {
                jobq = Some(scheduler.every(day).at_time(time));
            }
        }

        if let Some(jobq) = jobq {
            if dump {
                println!("{:#?}", jobq);
            }

            let mut client = client.clone();
            jobq.run(move || {
                for (room, temp) in profile.temperature.iter() {
                    match set_temperature(&mut client, room, *temp) {
                        Ok(_) => debug!("temperature in {} set to {}", room, temp),
                        Err(e) => warn!("could not set temperature: {}", e),
                    }
                }
            });
        }
    };
}

fn set_temperature(
    client: &mut Client,
    friendly_name: &str,
    temperature: f32,
) -> Result<(), ClientError> {
    let json = format!(
        r#"{{"system_mode":"heat", "current_heating_setpoint":{}}}"#,
        temperature
    );
    let topic = format!("zigbee2mqtt/{}/set", friendly_name);
    println!("publish at {}: {}", topic, json);
    client.publish(topic, QoS::AtMostOnce, false, json.as_bytes())
}
